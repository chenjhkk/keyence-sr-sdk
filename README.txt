==================================================================
Disclaimer
==================================================================
Because it is a sample program for the operation check connection with the code reader, Keyence Corporation (hereinafter referred to as Keyence) does not guarantee this behavior for any program. In addition, even if there is a bug in the program, Keyence assumes no obligation to fix it and any other maintenance work. For the product and the results that have occurred in this program, Keyence is not intended to guarantee. In addition, as well as damage caused by the use of this program, for damage caused directly or indirectly to any third party, Keyence shall not be liable regardless of the legal basis, and you shall not make any claim.


==================================================================
Revision history
2017/10/20	Ver 1.00	Release
2020/6/1	Ver 2.00	Release
==================================================================
